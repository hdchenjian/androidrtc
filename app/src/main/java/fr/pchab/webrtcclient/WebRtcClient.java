package fr.pchab.webrtcclient;

import java.lang.Thread;
import java.sql.Timestamp;
import java.net.URI;
import java.net.URISyntaxException;
import android.util.Base64;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_10;
import org.java_websocket.handshake.ServerHandshake;

import org.json.JSONException;
import org.json.JSONObject;

import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.Camera1Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.DataChannel;
import org.webrtc.EglBase;
import org.webrtc.IceCandidate;
import org.webrtc.Logging;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

public class WebRtcClient {
    private final static String TAG = WebRtcClient.class.getCanonicalName();
    private PeerConnectionFactory factory;
    private LinkedList<PeerConnection.IceServer> iceServers = new LinkedList<>();
    private PeerConnectionParameters peerConnectionParameters;
    private MediaConstraints pcConstraints = new MediaConstraints();

    private MediaStream localMediaStream;
    private VideoTrack localVideoTrack;
    private AudioTrack localAudioTrack;

    private VideoSource videoSource;
    private RtcListener mListener;
    private WebSocketClient websocketClient;
    private boolean loginInSuccess = false;
    private String userName;
    private String peerName;
    private Peer peer;
    public boolean connectEstablish = false;
    private CameraVideoCapturer videoCapturer;
    private boolean videoCapturerStopped;
    private final ScheduledExecutorService executor;
    private static final WebRtcClient instance = new WebRtcClient();
    private PeerConnection pc;
    private String turn_key = "t=u408021891333";
    private String host;

    /* Implement this interface to be notified of events. */
    public interface RtcListener{
        void onStatusChanged(String newStatus);
        void onAddRemoteStream(MediaStream remoteStream);
        void onRemoveRemoteStream();
        void onPeerLeave();
    }

    private void handleLogin(JSONObject payload) throws JSONException {
        Log.d(TAG, "handleLogin");
        boolean login_result = payload.getBoolean("success");
        System.out.println(login_result);
        if(login_result == true){
            Log.d(TAG, "LoginCommand: success");
            loginInSuccess = true;
        } else{
            Log.d(TAG, "Ooops...try a different username");
            //loginInSuccess = false;
        }
    }

    private void handleOffer(String peerId, final JSONObject payload) throws JSONException {
        Log.d(TAG,"handleOffer");
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    SessionDescription sdp = new SessionDescription(
                            SessionDescription.Type.fromCanonicalForm(payload.getString("type")),
                            payload.getString("sdp")
                    );
                    pc.setRemoteDescription(peer, sdp);
                    pc.createAnswer(peer, pcConstraints);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void handleAnswer(String peerId, final JSONObject payload) throws JSONException {
        Log.d(TAG,"handleAnswer");
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    SessionDescription sdp = new SessionDescription(
                            SessionDescription.Type.fromCanonicalForm(payload.getString("type")),
                            payload.getString("sdp")
                    );
                    pc.setRemoteDescription(peer, sdp);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void handleCandidate(String peerId, final JSONObject payload) throws JSONException {
        Log.d(TAG,"handleCandidate");
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    if (pc.getRemoteDescription() != null) {
                        IceCandidate candidate = new IceCandidate(
                                payload.getString("sdpMid"),
                                payload.getInt("sdpMLineIndex"),
                                payload.getString("candidate"));
                        pc.addIceCandidate(candidate);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void handleLeave() {
        mListener.onPeerLeave();
    }

    /**
     * Send a message through the signaling server
     *
     * @param to id of recipient
     * @param type type of message
     * @param payload payload of message
     * @throws JSONException
     */
    public void sendMessage(String to, String type, JSONObject payload) throws JSONException {
        JSONObject message = new JSONObject();
        message.put("name", to);
        message.put("type", type);
        message.put("payload", payload);
        websocketClient.send(message.toString());
    }

    private void getMessage(JSONObject data){
        try {
            String type = data.getString("type");
            String from = data.optString("from");
            JSONObject payload = null;
            payload = data.optJSONObject("payload");
            // if peer is unknown, try to add him
            if((from != "") && (peerName == null || !peerName.equals(from))) {
                Log.d(TAG,"peerName != from:" + peerName + from);
                addPeer(from);
            }
            switch(type){
            case "login":
                handleLogin(data);
		        break;
            case "offer":
                handleOffer(from, payload);
                break;
            case "answer":
                handleAnswer(from, payload);
                break;
            case "candidate":
                handleCandidate(from, payload);
                break;
            case "leave":
                handleLeave();
                break;
            default:
                break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class Peer implements SdpObserver, PeerConnection.Observer{
        /* Called on success of Create{Offer,Answer}(). implements SdpObserver interface*/
        @Override
        public void onCreateSuccess(final SessionDescription sdp) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        String sdpString = sdp.description;
                        sdpString = preferCodec(sdpString, peerConnectionParameters.videoCodec, false);
                        sdpString = preferCodec(sdpString, peerConnectionParameters.audioCodec, true);
                        JSONObject payload = new JSONObject();
                        payload.put("type", sdp.type.canonicalForm());
                        payload.put("sdp", sdpString);
                        sendMessage(peerName, sdp.type.canonicalForm(), payload);
                        executor.execute(new Runnable() {
                            @Override
                            public void run() {
                                pc.setLocalDescription(Peer.this, sdp);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }

        @Override
        public void onSetSuccess() {}
        @Override
        public void onCreateFailure(String s) {System.out.println("\n\n\nfailed:\n");System.out.println(s);}
        @Override
        public void onSetFailure(String s) {}


        /* implement PeerConnection.Observer interface */
        @Override
        public void onSignalingChange(PeerConnection.SignalingState signalingState) {}

        @Override
        public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
            if(iceConnectionState == PeerConnection.IceConnectionState.DISCONNECTED) {
                removePeer();
                mListener.onStatusChanged("DISCONNECTED");
            }
        }

        @Override
        public void onIceConnectionReceivingChange(boolean var1) {};

        @Override
        public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {}

        @Override
        public void onIceCandidate(final IceCandidate candidate) {
            try {
                JSONObject payload = new JSONObject();
                payload.put("sdpMLineIndex", candidate.sdpMLineIndex);
                payload.put("sdpMid", candidate.sdpMid);
                payload.put("candidate", candidate.sdp);
                sendMessage(peerName, "candidate", payload);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onIceCandidatesRemoved(IceCandidate[] var1) {};

        @Override
        public void onAddStream(MediaStream mediaStream) {
            Log.d(TAG,"onAddStream "+mediaStream.label());
            // remote streams are displayed from 1 to MAX_PEER (0 is localStream)
            mListener.onAddRemoteStream(mediaStream);
        }

        @Override
        public void onRemoveStream(MediaStream mediaStream) {
            Log.d(TAG,"onRemoveStream "+mediaStream.label());
            removePeer();
            mListener.onRemoveRemoteStream();
        }

        @Override
        public void onDataChannel(DataChannel dataChannel) {}

        @Override
        public void onRenegotiationNeeded() {

        }

        public Peer() {
            Log.d(TAG,"new Peer: " + peerName);
            mListener.onStatusChanged("CONNECTING");
        }

        private String preferCodec(String sdpDescription, String codec, boolean isAudio) {
            String[] lines = sdpDescription.split("\r\n");
            int mLineIndex = -1;
            String codecRtpMap = null;
            // a=rtpmap:<payload type> <encoding name>/<clock rate> [/<encoding parameters>]
            String regex = "^a=rtpmap:(\\d+) " + codec + "(/\\d+)+[\r]?$";
            Pattern codecPattern = Pattern.compile(regex);
            String mediaDescription = "m=video ";
            if (isAudio) {
                mediaDescription = "m=audio ";
            }
            for (int i = 0; (i < lines.length)
                    && (mLineIndex == -1 || codecRtpMap == null); i++) {
                if (lines[i].startsWith(mediaDescription)) {
                    mLineIndex = i;
                    continue;
                }
                Matcher codecMatcher = codecPattern.matcher(lines[i]);
                if (codecMatcher.matches()) {
                    codecRtpMap = codecMatcher.group(1);
                }
            }
            if (mLineIndex == -1) {
                Log.w(TAG, "No " + mediaDescription + " line, so can't prefer " + codec);
                return sdpDescription;
            }
            if (codecRtpMap == null) {
                Log.w(TAG, "No rtpmap for " + codec);
                return sdpDescription;
            }
            Log.d(TAG, "Found " +  codec + " rtpmap " + codecRtpMap + ", prefer at "
                    + lines[mLineIndex]);
            String[] origMLineParts = lines[mLineIndex].split(" ");
            if (origMLineParts.length > 3) {
                StringBuilder newMLine = new StringBuilder();
                int origPartIndex = 0;
                // Format is: m=<media> <port> <proto> <fmt> ...
                newMLine.append(origMLineParts[origPartIndex++]).append(" ");
                newMLine.append(origMLineParts[origPartIndex++]).append(" ");
                newMLine.append(origMLineParts[origPartIndex++]).append(" ");
                newMLine.append(codecRtpMap);
                for (; origPartIndex < origMLineParts.length; origPartIndex++) {
                    if (!origMLineParts[origPartIndex].equals(codecRtpMap)) {
                        newMLine.append(" ").append(origMLineParts[origPartIndex]);
                    }
                }
                lines[mLineIndex] = newMLine.toString();
                Log.d(TAG, "Change media description: " + lines[mLineIndex]);
            } else {
                Log.e(TAG, "Wrong SDP media description format: " + lines[mLineIndex]);
            }
            StringBuilder newSdpDescription = new StringBuilder();
            for (String line : lines) {
                newSdpDescription.append(line).append("\r\n");
            }
            return newSdpDescription.toString();
        }
    }

    private void addPeer(String id) {
        peerName = id;
        executor.execute(new Runnable() {
            @Override
            public void run() {
                peer = new Peer();
                int time_to_live = 600;  //10 minutes;
                Timestamp ts = new Timestamp(System.currentTimeMillis());
                String coturnUserName = Integer.toString((int)(Math.floor(ts.getTime() / 1000)) + time_to_live);
                coturnUserName += (":" + userName);
                String password = "";
                try {
                    password = hmacSha1Encrypt(coturnUserName, turn_key);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                String server_url = "turn:" + host;
                iceServers.add(new PeerConnection.IceServer(server_url + ":3478?transport=udp", coturnUserName, password));
                iceServers.add(new PeerConnection.IceServer(server_url + ":3478?transport=tcp", coturnUserName, password));
                iceServers.add(new PeerConnection.IceServer(server_url + ":3479?transport=udp", coturnUserName, password));
                iceServers.add(new PeerConnection.IceServer(server_url + ":3479?transport=tcp", coturnUserName, password));
                pc = factory.createPeerConnection(iceServers, pcConstraints, peer);
                pc.addStream(localMediaStream);
            }
        });
    }

    private void removePeer() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                peer = null;
            }
        });
    }

    public static String hmacSha1Encrypt(String value, String key) throws Exception {
        byte[] keyBytes = key.getBytes();
        SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(signingKey);
        byte[] rawHmac = mac.doFinal(value.getBytes());
        return new String(Base64.encodeToString(rawHmac, Base64.NO_WRAP));
    }

    /**
     * Call this method in Activity.onPause()
     */
    public void onPause() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (videoCapturer != null && !videoCapturerStopped) {
                    Log.d(TAG, "Stop video source.");
                    try {
                        videoCapturer.stopCapture();
                    } catch (InterruptedException e) {}
                    videoCapturerStopped = true;
                }
            }
        });
    }

    /**
     * Call this method in Activity.onResume()
     */
    public void onResume() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (videoCapturer != null && videoCapturerStopped) {
                    Log.d(TAG, "Restart video source.");
                    videoCapturer.startCapture(peerConnectionParameters.videoWidth, peerConnectionParameters.videoHeight, peerConnectionParameters.videoFps);
                    videoCapturerStopped = false;
                }
            }
        });
    }

    /**
     * Call this method in Activity.onDestroy()
     */
    public void onDestroy() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                pc.dispose();
                peer = null;
                if(videoSource != null) {
                    videoSource.dispose();
                    videoSource = null;
                }
                if (factory != null) {
                    factory.dispose();
                    factory = null;
                }
                websocketClient.close();
            }
        });
    }

    private void login(){
        Random rand = new Random();
        while (!loginInSuccess){
            int  n = rand.nextInt(100);
            userName = Integer.toString(n);
            try {
                JSONObject payload = new JSONObject();
                payload.put("type", "login");
                payload.put("name", userName);
                websocketClient.send(payload.toString());
                Thread.sleep(500);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public int call(String peerId) {
        Log.d(TAG,"call other user");
        if(connectEstablish == false){
            return 1;
        }
        try {
            JSONObject payload = new JSONObject();
            payload.put("type", "offer-loopback");
            payload.put("name", peerId);
            websocketClient.send(payload.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /* Start the client. Set up the local stream and login. */
    public void start(final EglBase.Context renderEGLContext){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                setCamera(renderEGLContext);
            }
        });
        login();
    }

    private void setCamera(EglBase.Context renderEGLContext){
        localMediaStream = factory.createLocalMediaStream("ARDAMS");
        /*
        if(peerConnectionParameters.videoCallEnabled){
            factory.setVideoHwAccelerationOptions(renderEGLContext, renderEGLContext);
            videoCapturer = createCapturer(new Camera1Enumerator(peerConnectionParameters.captureToTexture));
            videoSource = factory.createVideoSource(videoCapturer); //, videoConstraints);
            videoCapturer.startCapture(peerConnectionParameters.videoWidth, peerConnectionParameters.videoHeight, peerConnectionParameters.videoFps);
            localVideoTrack = factory.createVideoTrack("ARDAMSv0", videoSource);
            localMediaStream.addTrack(localVideoTrack);
        }

        AudioSource audioSource = factory.createAudioSource(new MediaConstraints());
        localAudioTrack = factory.createAudioTrack("ARDAMSa0", audioSource);
        //localAudioTrack.setEnabled(enableAudio);
        localMediaStream.addTrack(localAudioTrack); */
    }


    private CameraVideoCapturer createCapturer(CameraEnumerator enumerator) {
        CameraVideoCapturer videoCapturer = null;
        final String[] deviceNames = enumerator.getDeviceNames();

        // First, try to find front facing camera
        Logging.d(TAG, "Looking for front facing cameras.");
        for (String deviceName : deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                Logging.d(TAG, "Creating front facing camera capturer.");
                videoCapturer = enumerator.createCapturer(deviceName, null);
                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }

        // Front facing camera not found, try something else
        Logging.d(TAG, "Looking for other cameras.");
        for (String deviceName : deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                Logging.d(TAG, "Creating other camera capturer.");
                videoCapturer = enumerator.createCapturer(deviceName, null);

                if (videoCapturer != null) {
                    return videoCapturer;
                }
            }
        }
        return null;
    }


    public static WebRtcClient getInstance() {
        return instance;
    }

    private WebRtcClient() {
        // Executor thread is started once in private ctor and is used for all
        // peer connection API calls to ensure new peer connection factory is
        // created on the same thread as previously destroyed factory.
        executor = Executors.newSingleThreadScheduledExecutor();
    }

    public void createWebRtcClientFactory(final RtcListener listener, final String host, PeerConnectionParameters params, String host_address) {
        this.host = host_address;
        videoCapturerStopped = false;
        mListener = listener;
        peerConnectionParameters = params;
        pcConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveAudio", "false"));
        pcConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"));
        pcConstraints.optional.add(new MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"));

        executor.execute(new Runnable() {
            @Override
            public void run() {
                createWebRtcClientFactoryInternal(listener, host);
            }
        });
    }


    private void createWebRtcClientFactoryInternal(RtcListener listener, String host) {
        PeerConnectionFactory.initializeInternalTracer();
        // Initialize field trials.
        PeerConnectionFactory.initializeFieldTrials("");

        PeerConnectionFactory.initializeAndroidGlobals(listener, true, true,
                peerConnectionParameters.videoCodecHwAcceleration);
        PeerConnectionFactory.Options options = new PeerConnectionFactory.Options();
        factory = new PeerConnectionFactory(options);
        try {
            websocketClient = new WebSocketClient(new URI(host), new Draft_10()) {
                public void onMessage(String message) {
                    System.out.println("got: " + message + "\n");
                    try {
                        JSONObject data = new JSONObject(message);
                        getMessage(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onOpen(ServerHandshake handshake) {
                    System.out.println("You are connected to ChatServer: " + getURI() + "\n");
                }

                @Override
                public void onClose(int code, String reason, boolean remote) {
                    System.out.println("You have been disconnected from: " + getURI() + "; Code: " + code + " " + reason + "\n");
                }

                @Override
                public void onError(Exception ex) {
                    System.out.println("Exception occured ...\n" + ex + "\n");
                    ex.printStackTrace();
                }
            };
            connectEstablish = websocketClient.connectBlocking();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

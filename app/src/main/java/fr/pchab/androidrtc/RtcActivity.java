package fr.pchab.androidrtc;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.Context;
import android.content.Intent;
import android.app.PendingIntent;
import android.app.AlarmManager;
import android.graphics.Point;
import android.view.KeyEvent;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.webrtc.EglBase;
import org.webrtc.MediaStream;
import org.webrtc.RendererCommon.ScalingType;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoRenderer;

import fr.pchab.webrtcclient.AppRTCAudioManager;
import fr.pchab.webrtcclient.WebRtcClient;
import fr.pchab.webrtcclient.PeerConnectionParameters;

public class RtcActivity extends Activity implements WebRtcClient.RtcListener {
    private final static String TAG = RtcActivity.class.getCanonicalName();
    private static final String VIDEO_CODEC_VP9 = "H264";
    private static final String AUDIO_CODEC_OPUS = "opus";
    // Remote video screen position
    private static final int REMOTE_X = 0;
    private static final int REMOTE_Y = 0;
    private static final int REMOTE_WIDTH = 100;
    private static final int REMOTE_HEIGHT = 100;
    private ScalingType scalingType = ScalingType.SCALE_ASPECT_FILL;
    private SurfaceViewRenderer remoteRender;
    private PercentFrameLayout remoteRenderLayout;
    private EglBase rootEglBase;

    private WebRtcClient client;
    private String keyprefRoom;
    private EditText roomEditText;
    private ImageButton connectButton;
    private ImageButton disconnectButton;
    private SharedPreferences sharedPref;
    private AppRTCAudioManager audioManager;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.main);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        keyprefRoom = getString(R.string.pref_room_key);
        roomEditText = (EditText) findViewById(R.id.room_edittext);
        roomEditText.requestFocus();
        connectButton = (ImageButton) findViewById(R.id.connect_button);
        connectButton.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (client != null) {
                            int ret = client.call(roomEditText.getText().toString());
                            if (ret != 0) {
                                Toast.makeText(view.getContext(), "system is busy, please try a later",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
        disconnectButton = (ImageButton) findViewById(R.id.disconnect_button);
        disconnectButton.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        RtcActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    Builder builder = new Builder(RtcActivity.this);
                                    builder.setMessage("确定退出?");
                                    builder.setTitle("提示");
                                    builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                disconnect();
                                                android.os.Process.killProcess(android.os.Process.myPid());
                                            }
                                        });
                                    builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                    builder.create().show();
                                }
                            });
                    }
                });

        // Create UI controls.
        remoteRender = (SurfaceViewRenderer) findViewById(R.id.remote_video_view);
        // Create video renderers.
        rootEglBase = EglBase.create();
        remoteRender.init(rootEglBase.getEglBaseContext(), null);
        remoteRenderLayout = (PercentFrameLayout) findViewById(R.id.remote_video_layout);
        updateVideoView();

        audioManager = AppRTCAudioManager.create(this, new Runnable() {
                    // This method will be called each time the audio state (number and
                    // type of devices) has been changed.
                    @Override
                    public void run() {
                        onAudioManagerChangedState();
                    }
                }
        );
        // Store existing audio settings and change audio mode to
        // MODE_IN_COMMUNICATION for best possible VoIP performance.
        Log.d("TAG", "Initializing the audio manager...");
        audioManager.init();


        String mSocketAddress;
        String host_address = getResources().getString(R.string.host);
        mSocketAddress = "ws://" + host_address;
        mSocketAddress += (":" + getResources().getString(R.string.port) + "/");
        Point displaySize = new Point();
        getWindowManager().getDefaultDisplay().getSize(displaySize);
        PeerConnectionParameters params = new PeerConnectionParameters(
                true, false, displaySize.x, displaySize.y, 15, 1, VIDEO_CODEC_VP9, true, 1, AUDIO_CODEC_OPUS, true, true);
        client = WebRtcClient.getInstance();
        client.createWebRtcClientFactory(this, mSocketAddress, params, host_address); //, localRender, remoteRender);
        while (!client.connectEstablish) {
            System.out.println("can not connect to server.");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        client.start(rootEglBase.getEglBaseContext());
        client.onResume();
    }

    private void onAudioManagerChangedState() {}

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Log.d(TAG, "onKeyDown, KEYCODE_BACK");
            Builder builder = new Builder(this);
            builder.setTitle("提示");
            builder.setMessage("确定退出?");
            builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    disconnect();
                    android.os.Process.killProcess(android.os.Process.myPid());
                }
            });
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.create().show();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onPause() {
        super.onPause();
        String room = roomEditText.getText().toString();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(keyprefRoom, room);
        editor.commit();
        if (client != null) {
            client.onPause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        String room = sharedPref.getString(keyprefRoom, "");
        roomEditText.setText(room);
        Log.d("connectListener", "connectListener");
    }
    // Disconnect from remote resources, dispose of local resources, and exit.
    private void disconnect() {
        if (client != null) {
            client.onDestroy();
        }
        if (remoteRender != null) {
            remoteRender.release();
            remoteRender = null;
        }
        if (audioManager != null) {
            audioManager.close();
            audioManager = null;
        }
    }
    @Override
    public void onDestroy() {
        rootEglBase.release();
        disconnect();
        super.onDestroy();
    }

    @Override
    public void onStatusChanged(final String newStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), newStatus, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onAddRemoteStream(MediaStream remoteStream) {
        remoteStream.videoTracks.get(0).addRenderer(new VideoRenderer(remoteRender));
        updateVideoView();
    }

    @Override
    public void onRemoveRemoteStream() {}

    @Override
    public void onPeerLeave(){
        RtcActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    Builder builder = new Builder(RtcActivity.this);
                    builder.setMessage("对方已关闭连接，将退出应用");
                    builder.setTitle("提示");
                    builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                disconnect();
                                android.os.Process.killProcess(android.os.Process.myPid());
                                //restart(getApplicationContext(), 500);
                            }
                        });
                    builder.create().show();
                }
            });
        updateVideoView();
    }

    private void restart(Context context, int delay) {
        Log.e("", "restarting app");
        Intent restartIntent = context.getPackageManager()
            .getLaunchIntentForPackage(context.getPackageName() );
        PendingIntent intent = PendingIntent.getActivity(
            context, 0,
            restartIntent, Intent.FLAG_ACTIVITY_CLEAR_TOP);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC, System.currentTimeMillis() + delay, intent);
        RtcActivity.this.finish();
    }

    private void updateVideoView() {
        RtcActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                remoteRenderLayout.setPosition(REMOTE_X, REMOTE_Y, REMOTE_WIDTH, REMOTE_HEIGHT);
                remoteRender.setScalingType(scalingType);
                remoteRender.setMirror(false);
                remoteRender.requestLayout();
            }
        });
    }
}

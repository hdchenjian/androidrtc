# AndroidRTC

## WebRTC Live Streaming


It is designed to demonstrate WebRTC video calls between androids and/or desktop browsers, but WebRtcClient could be used in other scenarios. 
Build with Android Studio 2.2. The Intellij IDEA version is in the master branch.
You can import the webrtc-client module in your own app if you want to work with it.


## How To

Run the signaling server: node collider_server.js 
Then you can launch the app.


## Libraries

### [libjingle peerconnection](https://developers.google.com/talk/libjingle/developer_guide)
### [Java-WebSocket](https://github.com/TooTallNate/Java-WebSocket)

refer:
###[ProjectRTC](https://github.com/pchab/ProjectRTC)
###[pchab/AndroidRTC](https://github.com/pchab/AndroidRTC)
